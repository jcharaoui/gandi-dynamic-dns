# gandi-dynamic-dns

An OpenWRT/LEDE hotplug script to update DNS records using the Gandi v5 Live DNS
API

Relying on the hotplug functionality avoids the need to query the API every few
minutes, using cron for example. Instead, only when the device's WAN interface
is brought up, or receives a new IP address through DHCP, the script is called.

### Prerequisites

- An OpenWRT/LEDE device connected to the Internet on the WAN interface
- A [Gandi](https://gandi.net) v5 account with an
[API key](http://doc.livedns.gandi.net/#step-1-get-your-api-key)

### Installing

- Log on to your OpenWRT/LEDE device
- Install the required software dependencies:

```
opkg update
opkg install curl ca-bundle
```

- Download the script to the `/etc/hotplug.d/iface` directory and grant execute
permission:

```
curl https://gitlab.com/jcharaoui/gandi-dynamic-dns/raw/HEAD/gandi-dynamic-dns \
 > /etc/hotplug.d/iface/30-gandi-dynamic-dns
chmod +x /etc/hotplug.d/iface/30-gandi-dynamic-dns
```

- Insert your API key, domain and subdomain into the script configuration:

```
nano /etc/hotplug.d/iface/30-gandi-dynamic-dns
```

- (Optional) Edit the `sysupgrade.conf` file to keep the script between
upgrades:

```
echo "/etc/hotplug.d/iface/30-gandi-dynamic-dns" >> /etc/sysupgrade.conf
```

### Test run

The command below may be used to test if the script works correctly. Replace the
device name with the correct WAN ethernet interface for your device.

```
DEVICE=ethX INTERFACE=wan ACTION=ifup /etc/hotplug.d/iface/30-gandi-dyndns
```

### Logs

The script logs its output to the system log. You may read its entries using the
command below:

```
logread | grep gandi-dynamic-dns
```

## License

This project is licensed under the GPLv3 License - see the [LICENSE](LICENSE)
file for details
